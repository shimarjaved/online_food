<?php 

namespace app\helpers;

class TaxHelper
{
	public static function calculateTax($percentage,$amount)
	{
		return ceil(($percentage/100) * $amount);
	}
	
}