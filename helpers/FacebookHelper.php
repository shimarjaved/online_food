<?php

namespace app\helpers;

class FacebookHelper
{
	public function getAcTok($helper)
	{
		try {
			if (isset($_SESSION['facebook_access_token'])) 
			{
				$accessToken = $_SESSION['facebook_access_token'];
			} else {
		  		$accessToken = $helper->getAccessToken();
			}
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		 	// When Graph returns an error
		 	echo 'Graph returned an error: ' . $e->getMessage();

		  	exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		 	// When validation fails or other local issues
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  	exit;
		}
		return $accessToken;
	}

		
}