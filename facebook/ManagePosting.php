<?php

require_once __DIR__ . '/src/Facebook/autoload.php';

class ManagePosting 
{
	public $appId = '303629483449045';
	public $appSecret = '4eaf117086e9050c5c2efad95d6f1445'; 
	public $permissions = ['email','publish_actions'];

	public function getFbObject()
	{
		$fb = new Facebook\Facebook([
		  'app_id' => $this->appId,
		  'app_secret' => $this->appSecret,
		  'default_graph_version' => 'v2.10',
		  'default_access_token' => $this->appId.'|'.$this->appSecret,
	  	]);

	  	return $fb;
	}

	public function getHelper()
	{
		$fb = $this->getFbObject();
		$helper = $fb->getRedirectLoginHelper();
		return $helper;
	}

	public function getAcTok()
	{
		$helper = $this->getHelper();
		try {
			if (isset($_SESSION['facebook_access_token'])) 
			{
				$accessToken = $_SESSION['facebook_access_token'];
			} else {
		  		$accessToken = $helper->getAccessToken();
			}
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		 	// When Graph returns an error
		 	echo 'Graph returned an error: ' . $e->getMessage();

		  	exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		 	// When validation fails or other local issues
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  	exit;
		}
		return $accessToken;
	}

	public function getProfile()
	{
		$accessToken = $this->getAcTok();
		if (isset($accessToken)) 
		{
			if (isset($_SESSION['facebook_access_token'])) 
			{
				$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
			} 
			else
			{
				// getting short-lived access token
				$_SESSION['facebook_access_token'] = (string) $accessToken;

			  	// OAuth 2.0 client handler
				$oAuth2Client = $fb->getOAuth2Client();

				// Exchanges a short-lived access token for a long-lived one
				$longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);

				$_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;

				// setting default access token to be used in script
				$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
			}

			// redirect the user back to the same page if it has "code" GET variable
			if (isset($_GET['code'])) {
				header('Location: ./');
			}

			// getting basic info about user
			try {
				$profile_request = $fb->get('/me?fields=name,first_name,last_name,email');
				$profile = $profile_request->getGraphNode()->asArray();
			} catch(Facebook\Exceptions\FacebookResponseException $e) {
				// When Graph returns an error
				echo 'Graph returned an error: ' . $e->getMessage();
				session_destroy();
				// redirecting user back to app login page
				header("Location: ./");
				exit;
			} catch(Facebook\Exceptions\FacebookSDKException $e) {
				// When validation fails or other local issues
				echo 'Facebook SDK returned an error: ' . $e->getMessage();
				exit;
			}
			// printing $profile array on the screen which holds the basic info about user
			return $profile;
		}
	}

	public function postWIthImage($message,$filePath)
	{
		$fb = $this->getFbObject();
		$data = [
		  'message' => $message,
		  'source' => $fb->fileToUpload($filePath),
		];
		$accessToken = $this->getAcTok();

		try {
		  // Returns a `Facebook\FacebookResponse` object
		  $response = $fb->post('/me/photos', $data, '{access-token}');
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		  echo 'Graph returned an error: ' . $e->getMessage();
		  exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		  echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  exit;
		}
		$graphNode = $response->getGraphNode();
		echo 'Photo ID: ' . $graphNode['id'];
	}


}
?>