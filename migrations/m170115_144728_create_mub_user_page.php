<?php

namespace app\migrations;
use app\commands\Migration;

class m170115_144728_create_mub_user_page extends Migration
{
    public function getTableName()
    {
        return 'mub_user_page';
    }
    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id'],
        ];
    }

    public function getKeyFields()
    {
        return [
            'page_title' => 'page_title',
            'url' => 'url',
            'page_type' => 'page_type'
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer(11)->notNull(),
            'page_title' => $this->string(100)->defaultValue(NULL),
            'controller' => $this->string(50)->notNull(),
            'action' => $this->string(100)->notNull(),
            'url' => $this->string(100)->notNull(),
            'page_type' => "enum('0','1') NOT NULL COMMENT '0-Normal_Page,1-Blog_page DEFAULT 0' DEFAULT '0'",
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
