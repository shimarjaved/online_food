<?php

namespace app\migrations;
use app\commands\Migration;

class m170430_055660_create_service extends Migration
{
    public function getTableName()
    {
        return 'service';
    }
    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'service_name' => 'service_name',
            'service_slug'  =>  'service_slug',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->notNull(),
            'service_type' => "enum('Basic','Special') NOT NULL  DEFAULT 'Basic'",
            'service_name' => $this->string()->notNull(),
            'service_slug' => $this->string()->notNull(),
            'service_price' => $this->integer(),
            'status' => "enum('active','inactive') NOT NULL DEFAULT 'inactive'",
            'icon_url' => $this->string(),
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }
}
