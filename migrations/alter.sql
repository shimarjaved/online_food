ALTER TABLE `property` ADD `roomcount_show` INT NOT NULL DEFAULT '1' AFTER `property_area`, ADD `parking_show` INT NOT NULL DEFAULT '1' AFTER `roomcount_show`, ADD `bathrooms_show` INT NOT NULL DEFAULT '1' AFTER `parking_show`;

ALTER TABLE `property` ADD `area_show` VARCHAR(255) NOT NULL AFTER `bathrooms_show`;