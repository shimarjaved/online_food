<?php

namespace app\migrations;
use app\commands\Migration;

class m170430_060042_create_restaurant_services extends Migration
{
    public function getTableName()
    {
        return 'restaurant_services';
    }

    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id'],
            'restaurant_id' => ['restaurant','id'],
            'service_id' => ['service','id']
        ];
    }

    public function getKeyFields()
    {
        return [];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->notNull(),
            'restaurant_id' => $this->integer()->notNull(),
            'service_id' => $this->integer()->notNull(),
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }
}
