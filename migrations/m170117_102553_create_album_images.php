<?php

namespace app\migrations;
use app\commands\Migration;

class m170117_102553_create_album_images extends Migration
{
    public function getTableName()
    {
        return 'album_images';
    }
    public function getForeignKeyFields()
    {
        return [
            'album_id' => ['mub_user_album', 'id'],
            'image_id' => ['mub_user_images','id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'album_id' => 'album_id',
            'image_id'  =>  'image_id',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'album_id' => $this->integer()->defaultValue(NULL),
            'image_id' => $this->integer()->defaultValue(NULL),
            'created_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
