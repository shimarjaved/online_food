<?php

namespace app\migrations;
use app\commands\Migration;

class m170430_055709_create_menu extends Migration
{
   
    public function getTableName()
    {
        return 'menu';
    }
    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id'],
            'restaurant_id' => ['restaurant','id']
        ];
    }

    
    public function getKeyFields()
    {
        return [
            'menu_name' => 'menu_name',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->notNull(),
            'restaurant_id' => $this->integer()->notNull(),
            'menu_name' => $this->string(),
            'menu_slug' => $this->string(),
            'status' => "enum('active','inactive') NOT NULL DEFAULT 'inactive'",
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }
}
