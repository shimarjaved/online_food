<?php

namespace app\migrations;
use app\commands\Migration;

class m170430_055658_create_restaurant extends Migration
{
    public function getTableName()
    {
        return 'restaurant';
    }
    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id'],
            'state_id' => ['state','id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'restaurant_name' => 'restaurant_name',
            'restaurant_slug'  =>  'restaurant_slug',
            'city_name' => 'city_name'
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->notNull(),
            'locality_name' => $this->string(),
            'lat' => $this->string(),
            'long' => $this->string(),
            'city_name' => $this->string(),
            'state_id' => $this->integer()->notNull(),
            'restaurant_name' => $this->string()->notNull(),
            'restaurant_slug' => $this->string()->notNull(),
            'restaurant_type' => "enum('veg','non-veg','both') NOT NULL DEFAULT 'both'",
            'sa_a' => $this->string()->notNull(),
            'sa_b' => $this->string()->notNull(),
            'status' => "enum('active','inactive') NOT NULL DEFAULT 'inactive'",
            'description' => $this->text(),
            'pincode' => $this->integer(),
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }
}
