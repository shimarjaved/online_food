<?php

namespace app\modules\MubAdmin\modules\hotels\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\MubAdmin\modules\hotels\models\Tenant;

/**
 * TenantSearch represents the model behind the search form about `app\modules\MubAdmin\modules\hotels\models\Tenant`.
 */
class TenantSearch extends Tenant
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'mub_user_id', 'property_id', 'mobile', 'f_mobile', 'state_id', 'city_name', 'monthly_rent', 'security'], 'integer'],
            [['name', 'f_name', 'email', 'sa_home', 'sa_office', 'sa_permanent', 'joining_date', 'left_date', 'created_at', 'updated_at', 'del_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tenant::find()->where(['del_status' => '0']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'mub_user_id' => $this->mub_user_id,
            'property_id' => $this->property_id,
            'mobile' => $this->mobile,
            'f_mobile' => $this->f_mobile,
            'state_id' => $this->state_id,
            'city_name' => $this->city_name,
            'monthly_rent' => $this->monthly_rent,
            'security' => $this->security,
            'joining_date' => $this->joining_date,
            'left_date' => $this->left_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'f_name', $this->f_name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'sa_home', $this->sa_home])
            ->andFilterWhere(['like', 'sa_office', $this->sa_office])
            ->andFilterWhere(['like', 'sa_permanent', $this->sa_permanent])
            ->andFilterWhere(['like', 'del_status', $this->del_status]);

        return $dataProvider;
    }
}
