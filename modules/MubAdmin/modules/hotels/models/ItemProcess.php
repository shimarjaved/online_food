<?php 

namespace app\modules\MubAdmin\modules\hotels\models;
use app\components\Model;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;
use app\helpers\HtmlHelper;
use app\helpers\StringHelper;
use yii;

class ItemProcess extends Model
{
    public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $item = new Item();
        $itemCategory = new ItemCategory();
        $this->models = [
            'item' => $item,
            'itemCategory' => $itemCategory,
        ];
        return $this->models;
    }

    public function getFormData()
    {
        return[];
    }

    public function getSavedCategory($cat)
    {
        $category = [];
        foreach ($cat as $key => $amen) 
        {
            $category[] = $amen->category;
        }

        return $category;
    }
    public function getRelatedModels($model)
    {
        $itemCategory = $this->getSavedCategory($model->itemCategory);

        if(!empty($itemCategory))
        {
            $itemCategory = $model->itemCategory[0];
            $itemCategory->category= $itemCategory;
        }
        else
        {
            $itemCategory = new ItemCategory();
        }

        $this->relatedModels = [
            'item' => $model,
            'itemCategory' => $itemCategory
        ];
        return $this->relatedModels;
    }

    public function saveItemCategory($itemCategory,$itemId)
    {
        $selectedCategory = $itemCategory->category;
        $attribs = ['item_id','category','created_at'];
        $recordSet = [];
        foreach ($selectedCategory as $category) {
            $recordSet[] = [$itemId,$category,date('Y-m-d h:m:s',time())];
        }
        $amenCount = count($selectedCategory);

        $itemCategory->deleteAll(['item_id' => $itemId]);
        $inserted = Yii::$app->db->createCommand()->batchInsert($itemCategory->tableName(), $attribs, $recordSet)->execute();
        if($inserted == $amenCount)
        {
            return true;
        }
        p($itemCategory->getErrors());
    }

    public function saveImage($item)
    {
        $item->image_url = UploadedFile::getInstance($item,'image_url');
        $imageHelper = new \app\helpers\ImageUploader();
        $success = $imageHelper::uploadImages($item,'image_url');
        return $item;
    }
    public function saveItem($item)
    { 
        $item = $this->SaveImage($item);
        $menu = \Yii::$app->request->getQueryParam('menu');
        $userId = \app\models\User::getMubUserId();
        $item->mub_user_id =  $userId;
        $item->menu_id = $menu;
        $item->item_slug = StringHelper::generateSlug($item->item_name);
        return ($item->save()) ?$item->id :p($item->getErrors());
    }


     public function saveData($data = [])
    {
        if (isset($data['item'])&&
           isset($data['itemCategory']))
            {
            try {
                    $itemId = $this->saveItem($data['item']);
                    if($itemId)
                    {
                        $itemCategory = $this->saveItemCategory($data['itemCategory'],$itemId);
                        if($itemCategory)
                        {
                            return $itemId;
                        }
                        p($data['itemCategory']->getErrors());
                    }
                    p('item not saved');
                }
                catch (\Exception $e)
                {
                    throw $e;
                }
            } 
            else
            {
                throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
            }
    }
}