<?php

namespace app\modules\MubAdmin\modules\hotels\models;

use Yii;

use app\models\MubUser;

/**
 * This is the model class for table "service".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property string $service_type
 * @property string $service_name
 * @property string $service_slug
 * @property integer $service_price
 * @property string $status
 * @property string $icon_url
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property MubUser $mubUser
 */
class Service extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['service_name', 'icon_url','service_type'], 'required'],
            [['mub_user_id', 'service_price'], 'integer'],
            [['service_type', 'status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['service_name', 'service_slug', 'icon_url'], 'string', 'max' => 255],
            [['service_slug','service_name'], 'checkActive'],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'service_type' => 'Services Type',
            'service_name' => 'Services Name',
            'service_slug' => 'Services Slug',
            'service_price' => 'Services Price',
            'status' => 'Status',
            'icon_url' => 'Icon Url',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }
}
