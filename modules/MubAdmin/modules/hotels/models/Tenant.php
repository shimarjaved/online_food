<?php

namespace app\modules\MubAdmin\modules\hotels\models;

use Yii;
use app\models\MubUser;

/**
 * This is the model class for table "tenant".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property integer $property_id
 * @property string $name
 * @property string $f_name
 * @property string $email
 * @property integer $mobile
 * @property integer $f_mobile
 * @property integer $state_id
 * @property integer $city_name
 * @property integer $monthly_rent
 * @property integer $security
 * @property string $sa_home
 * @property string $sa_office
 * @property string $sa_permanent
 * @property string $joining_date
 * @property string $left_date
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property MubUser $mubUser
 * @property Property $property
 */
class Tenant extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tenant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'mobile'], 'required'],
            
            [['email'],'email'],
            [['mobile'],'number'],
            [['mobile'],'string', 'max'=> 10],


            [['mub_user_id', 'property_id', 'mobile', 'f_mobile', 'state_id',  'monthly_rent', 'security'], 'integer'],

            [['sa_home', 'sa_office','tenant_slug' ,'sa_permanent', 'del_status','city_name'], 'string'],

            [['left_date', 'created_at', 'updated_at'], 'safe'],

            [['name', 'f_name', 'email'], 'string', 'max' => 50],

            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
            
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => Property::className(), 'targetAttribute' => ['property_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'property_id' => 'Property ID',
            'name' => 'Name',
            'f_name' => 'Father Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'f_mobile' => 'Father Mobile',
            'state_id' => 'State Name',
            'city_name' => 'City Name',
            'monthly_rent' => 'Monthly Rent',
            'security' => 'Security',
            'sa_home' => 'Street Address Home',
            'tenant_slug' => 'Tenant Slug',
            'sa_office' => 'Street Address Office',
            'sa_permanent' => 'Street Address Permanent',
            'joining_date' => 'Joining Date',
            'left_date' => 'Left Date',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id' => 'property_id']);
    }
}
