<?php

namespace app\modules\MubAdmin\modules\hotels\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\MubAdmin\modules\hotels\models\Restaurant;

/**
 * RestaurantSearch represents the model behind the search form about `app\modules\MubAdmin\modules\hotels\models\Restaurant`.
 */
class RestaurantSearch extends Restaurant
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'mub_user_id', 'state_id', 'pincode'], 'integer'],
            [['lat', 'long', 'city_name', 'restaurant_name', 'restaurant_slug', 'restaurant_type', 'sa_a', 'sa_b', 'description','status', 'created_at', 'updated_at', 'del_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $mubUserId = \app\models\User::getMubUserId();
        $stateId = \Yii::$app->request->getQueryParam('state');
        $query = Restaurant::find()->where(['del_status' => '0','state_id' => $stateId]);

        if($mubUserId != '1')
        {
            $query->andWhere(['mub_user_id' => $mubUserId]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'mub_user_id' => $this->mub_user_id,
            'state_id' => $this->state_id,
            'pincode' => $this->pincode,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'lat', $this->lat])
            ->andFilterWhere(['like', 'long', $this->long])
            ->andFilterWhere(['like', 'city_name', $this->city_name])
            ->andFilterWhere(['like', 'restaurant_name', $this->restaurant_name])
            ->andFilterWhere(['like', 'restaurant_slug', $this->restaurant_slug])
            ->andFilterWhere(['like', 'restaurant_type', $this->restaurant_type])
            ->andFilterWhere(['like', 'sa_a', $this->sa_a])
            ->andFilterWhere(['like', 'sa_b', $this->sa_b])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'restaurant.status', $this->status])
            ->andFilterWhere(['like', 'del_status', $this->del_status]);

        return $dataProvider;
    }
}
