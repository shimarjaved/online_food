<?php

namespace app\modules\MubAdmin\modules\hotels\models;
use app\models\MubUser;
use Yii;


/**
 * This is the model class for table "restaurant_images_10".
 *
 * @Restaurant integer $id
 * @Restaurant integer $mub_user_id
 * @Restaurant integer $restaurant_id
 * @Restaurant string $title
 * @Restaurant string $description
 * @Restaurant string $url
 * @Restaurant string $thumbnail_url
 * @Restaurant string $thumbnail_path
 * @Restaurant string $full_path
 * @Restaurant string $active
 * @Restaurant string $keyword
 * @Restaurant integer $width
 * @Restaurant integer $height
 * @Restaurant integer $display_width
 * @Restaurant integer $display_hieght
 * @Restaurant string $type
 * @Restaurant string $created_at
 * @Restaurant string $updated_at
 * @Restaurant string $status
 * @Restaurant string $del_status
 *
 * @Restaurant MubUser $mubUser
 * @Restaurant Restaurant10 $Restaurant
 */
class RestaurantImages extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        $stateId = \Yii::$app->request->getQueryParam('state');
        if($stateId == '')
        {
            $stateId = \Yii::$app->request->getBodyParam('state');
        }
        return 'restaurant_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mub_user_id', 'restaurant_id', 'width', 'height', 'display_width', 'display_hieght'], 'integer'],
            [['title', 'description','thumbnail_url', 'full_path'], 'required'],
            [['url'], 'file', 'extensions' => 'jpg, png', 'mimeTypes' => 'image/jpeg, image/png','maxFiles' => 10],
            [['active', 'type', 'status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 50],
            [['description', 'url', 'thumbnail_url', 'thumbnail_path', 'full_path'], 'string', 'max' => 100],
            [['keyword'], 'string', 'max' => 255],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
            [['restaurant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Restaurant::className(), 'targetAttribute' => ['restaurant_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'restaurant_id' => 'Restaurant ID',
            'title' => 'Title',
            'description' => 'Description',
            'url' => 'Url',
            'thumbnail_url' => 'Thumbnail Url',
            'thumbnail_path' => 'Thumbnail Path',
            'full_path' => 'Full Path',
            'active' => 'Active',
            'keyword' => 'Keyword',
            'width' => 'Width',
            'height' => 'Height',
            'display_width' => 'Display Width',
            'display_hieght' => 'Display Hieght',
            'type' => 'Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurant()
    {
        return $this->hasOne(Restaurant10::className(), ['id' => 'restaurant_id']);
    }
}
