<?php 

namespace app\modules\MubAdmin\modules\hotels\models;
use app\components\Model;
use app\helpers\HtmlHelper;

class MenuProcess extends Model
{
    public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $menu = new Menu();
        $this->models = [
            'menu' => $menu
        ];
        return $this->models;
    }

    public function getFormData()
    {
        return [];
    }

    public function getRelatedModels($model)
    {
        $this->relatedModels = [
            'menu' => $model
        ];
        return $this->relatedModels;
    }

    public function saveMenu($menu)
    {
        if(\Yii::$app->controller->action->id =='create')
        {
            $userId = \app\models\User::getMubUserId();
        }
        else
        {
            $userId = $menu->mub_user_id;
        }
        $menu->mub_user_id =  $userId;
        $restaurant = new Restaurant();
        $forRestaurant = $restaurant::findOne($menu->restaurant_id);
        $menu->status = ($menu->status =='')? 'active' : $menu->status;
        $menu->menu_slug = \app\helpers\StringHelper::generateSlug($menu->menu_name);
        return ($menu->save()) ?$menu->id :p($menu->getErrors());
    }

    public function saveData($data = [])
    {
        if (isset($data['menu']))
            {
            try {
                    $menuId = $this->saveMenu($data['menu']);
                    if($menuId)
                    {
                        return $menuId;
                    }
                    p('menu not saved');
                }
                catch (\Exception $e)
                {
                    throw $e;
                }
            } 
            else
            {
                throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
            }
    }
}