<?php

namespace app\modules\MubAdmin\modules\hotels\models;

use Yii;

/**
 * This is the model class for table "item_category".
 *
 * @property integer $id
 * @property integer $item_id
 * @property string $category
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property Item $item
 */
class ItemCategory extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id'], 'integer'],
            [['del_status'], 'string'],
            [['created_at', 'updated_at','category'], 'safe'],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_id' => 'Item ID',
            'category' => 'Category',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }
}
