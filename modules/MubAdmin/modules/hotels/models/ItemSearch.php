<?php

namespace app\modules\MubAdmin\modules\hotels\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\MubAdmin\modules\hotels\models\Item;

/**
 * ItemSearch represents the model behind the search form about `app\modules\MubAdmin\modules\hotels\models\Item`.
 */
class ItemSearch extends Item
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'mub_user_id', 'menu_id', 'price', 'sell_price', 'discount_price'], 'integer'],
            [['item_name', 'item_slug', 'category_name', 'image_url', 'status', 'created_at', 'updated_at', 'del_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Item::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'mub_user_id' => $this->mub_user_id,
            'menu_id' => $this->menu_id,
            'price' => $this->price,
            'sell_price' => $this->sell_price,
            'discount_price' => $this->discount_price,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'item_name', $this->item_name])
            ->andFilterWhere(['like', 'item_slug', $this->item_slug])
            ->andFilterWhere(['like', 'category_name', $this->category_name])
            ->andFilterWhere(['like', 'image_url', $this->image_url])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'del_status', $this->del_status]);

        return $dataProvider;
    }
}
