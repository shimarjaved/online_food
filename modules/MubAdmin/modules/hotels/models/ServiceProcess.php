<?php 

namespace app\modules\MubAdmin\modules\hotels\models;
use app\components\Model;
use app\helpers\HtmlHelper;
use app\helpers\StringHelper;

class ServiceProcess extends Model
{
    public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $service = new Service();
        $this->models = [
            'service' => $service
        ];
        return $this->models;
    }

    public function getFormData()
    {
        return [];
    }

    public function getRelatedModels($model)
    {
        $this->relatedModels = [
            'service' => $model
        ];
        return $this->relatedModels;
    }

    public function saveService($service)
    {
        $userId = \app\models\User::getMubUserId();
        $service->mub_user_id =  $userId;
        $service->service_slug = StringHelper::generateSlug($service->service_name);
        return ($service->save()) ?$service->id :p($service->getErrors());
    }

    public function saveData($data = [])
    {
        if (isset($data['service']))
            {
            try {
                    $serviceId = $this->saveService($data['service']);
                    if($serviceId)
                    {
                        return $serviceId;
                    }
                    p('Service not saved');
                }
                catch (\Exception $e)
                {
                    throw $e;
                }
            } 
            else
            {
                throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
            }
    }
}