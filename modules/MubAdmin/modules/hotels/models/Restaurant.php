<?php

namespace app\modules\MubAdmin\modules\hotels\models;

use Yii;
use app\models\MubUser;
use app\models\State;

/**
 * This is the model class for table "restaurant".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property string $locality_name
 * @property string $lat
 * @property string $long
 * @property string $city_name
 * @property integer $state_id
 * @property string $restaurant_name
 * @property string $restaurant_slug
 * @property string $restaurant_type
 * @property string $sa_a
 * @property string $sa_b
 * @property string $status
 * @property string $description
 * @property integer $pincode
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property Menu[] $menus
 * @property MubUser $mubUser
 * @property State $state
 * @property RestaurantAmenities[] $restaurantAmenities
 * @property RestaurantImages[] $restaurantImages
 */
class Restaurant extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'restaurant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mub_user_id', 'state_id', 'pincode'], 'integer'],
            [['restaurant_type', 'status', 'description', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['locality_name', 'lat', 'long', 'city_name', 'restaurant_name', 'restaurant_slug', 'sa_a', 'sa_b'], 'string', 'max' => 255],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => State::className(), 'targetAttribute' => ['state_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'locality_name' => 'Locality Name',
            'lat' => 'Lat',
            'long' => 'Long',
            'city_name' => 'City Name',
            'state_id' => 'State ID',
            'restaurant_name' => 'Restaurant Name',
            'restaurant_slug' => 'Restaurant Slug',
            'restaurant_type' => 'Restaurant Type',
            'sa_a' => 'Sa A',
            'sa_b' => 'Sa B',
            'status' => 'Status',
            'description' => 'Description',
            'pincode' => 'Pincode',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenus()
    {
        return $this->hasMany(Menu::className(), ['restaurant_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(State::className(), ['id' => 'state_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurantServices()
    {
        return $this->hasMany(RestaurantServices::className(), ['restaurant_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurantImages()
    {
        return $this->hasMany(RestaurantImages::className(), ['restaurant_id' => 'id']);
    }
}
