<?php

namespace app\modules\MubAdmin\modules\hotels\models;

use Yii;
use app\models\MubUser;

/**
 * This is the model class for table "item".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property integer $menu_id
 * @property string $item_name
 * @property string $item_slug
 * @property integer $price
 * @property integer $sell_price
 * @property integer $discount_price
 * @property string $category_name
 * @property string $image_url
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property Allcategory[] $allcategories
 * @property Menu $menu
 * @property MubUser $mubUser
 */
class Item extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_name', 'price'], 'required'],
            [['mub_user_id', 'menu_id', 'price', 'sell_price', 'discount_price'], 'integer'],
            [['status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['item_name', 'item_slug', 'image_url'], 'string', 'max' => 255],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['menu_id' => 'id']],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'menu_id' => 'Menu ID',
            'item_name' => 'Item Name',
            'item_slug' => 'Item Slug',
            'price' => 'Price',
            'sell_price' => 'Sell Price',
            'discount_price' => 'Discount Price',
            // 'category_name' => 'Category Name',
            'image_url' => 'Image Url',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
   

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }

     /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemCategory()
    {
        return $this->hasMany(ItemCategory::className(), ['item_id' => 'id']);
    }
}
