<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\MubAdmin\modules\hotels\models\Category */

$this->title = 'Update Category: ' . $category->id;
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $category->id, 'url' => ['view', 'id' => $category->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="category-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'category' => $category,
    ]) ?>

</div>
