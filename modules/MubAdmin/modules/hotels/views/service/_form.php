<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $service app\modules\MubAdmin\modules\hotels\services\Service */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="service-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<div class="row">
<div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
    <?= $form->field($service, 'icon_url')->textInput(['maxlength' => true])->label('Icon Code');?>    

    <?= $form->field($service, 'service_type')->dropDownList([ 'Basic' => 'Basic', 'Special' => 'Special', ], ['prompt' => 'Select Service Type']);?>

    <?= $form->field($service, 'service_name')->textInput(['maxlength' => true]);?>
    <?php  $userRole = \Yii::$app->controller->getUserRole();
        if($userRole == 'admin' || $userRole == 'superadmin'){?>

    <?= $form->field($service, 'status')->dropDownList(['active' => 'Active','inactive' => 'InActive'], ['prompt' => 'Select Service Status']);?>
        
        <?php }?>

   <div class="form-group">
   <div class="col-md-12" style="text-align: center;" >
        <?= Html::submitButton($service->isNewRecord ? 'Create' : 'Update', ['class' => $service->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel', ['/mub-admin/hotels/service'], ['class' => 'btn btn-warning cancel'])?>
        </div>
    </div>
</div></div>
    <?php ActiveForm::end(); ?>

</div>
