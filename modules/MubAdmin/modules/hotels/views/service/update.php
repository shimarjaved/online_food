<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $service app\modules\MubAdmin\modules\hotels\services\Service */

$this->title = 'Update Service: ' . $service->id;
$this->params['breadcrumbs'][] = ['label' => 'Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $service->id, 'url' => ['view', 'id' => $service->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="service-update">
<div class="col-md-10 col-md-offset-1">
    <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'service' => $service,
    ]) ?>
</div>
