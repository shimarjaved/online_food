<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $service app\modules\MubAdmin\modules\hotels\services\Service */

$this->title = 'Service :'.  $service->service_name;
$this->params['breadcrumbs'][] = ['label' => 'Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="service-view">
<div class="col-md-10 col-md-offset-1">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
<div class="col-md-10 col-md-offset-1">
    <p>
        <?= Html::a('Delete', ['delete', 'id' => $service->id], ['class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('All Services', ['index'], ['class' => 'btn btn-primary']) ?>
    </p>
    </div>
<div class="col-md-6 col-md-offset-1">
    <?= DetailView::widget([
        'model' => $service,
        'attributes' => [
            'icon_url',
            'service_type',
            'service_name',
            'status'
            
        ],
    ]) ?>
</div>
</div>
