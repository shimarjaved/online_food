<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $menu app\modules\MubAdmin\modules\hotels\menus\Menu */

$this->title = 'Update Menu: ' . $menu->id;
$this->params['breadcrumbs'][] = ['label' => 'Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $menu->id, 'url' => ['view', 'id' => $menu->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="menu-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'menu' => $menu,
    ]) ?>

</div>
