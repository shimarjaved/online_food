<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $menu app\modules\MubAdmin\modules\hotels\menus\Menu */
/* @var $form yii\widgets\ActiveForm */
$restaurantId = \Yii::$app->request->getQueryParam('restaurant');
$stateId = \Yii::$app->request->getQueryParam('state');
if($stateId == '')
{
    $stateId = \Yii::$app->request->getBodyParam('state');
}
?>


<div class="menu-form">

    <?php $form = ActiveForm::begin(); ?>
     <div class="row">
        <div class="col-md-5 col-md-offset-1">
        <?= $form->field($menu, 'menu_name')->textInput(['maxlength' => true]) ?>
         </div>
         <div class="col-md-5">
        <?= $form->field($menu, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ], ['prompt' => 'Select Status']) ?>
      </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        <?= $form->field($menu,'restaurant_id')->hiddenInput(['value' => $restaurantId])->label(false);?>
         </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1 text-center">
         <div class="form-group">
        <?= Html::submitButton($menu->isNewRecord ? 'Create' : 'Update', ['class' => $menu->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
         </div>
      </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
