<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\MubAdmin\modules\hotels\models\Menu */

$this->title = 'Create Menu';
$this->params['breadcrumbs'][] = ['label' => 'Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-create">
	<div class="col-md-10 col-md-offset-1">

    <h1><?= Html::encode($this->title) ?></h1>
</div>
    <?= $this->render('_form', [
        'menu' => $menu,
    ]) ?>

</div>
