<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\MubAdmin\modules\hotels\models\MenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Menus';
$this->params['breadcrumbs'][] = $this->title;
$stateId = \Yii::$app->request->getQueryParam('state');
if($stateId == '')
{
    $stateId = \Yii::$app->request->getBodyParam('state');
}
$state = new app\models\State();
$currentState = $state::findOne($stateId);
$restaurantId = \Yii::$app->request->getQueryParam('restaurant');
?>

<div class="menu-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
         <?= Html::a('Create Menu', ['create','restaurant' => $restaurantId,'state' => $stateId], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'mub_user_id',
            'restaurant_id',
            'menu_name',
            'menu_slug',
            // 'status',
            // 'created_at',
            // 'updated_at',
            // 'del_status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
