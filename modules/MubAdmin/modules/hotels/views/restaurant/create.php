<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $restaurant app\modules\MubAdmin\modules\hotels\restaurants\restaurant */

$this->title = 'Create Restaurant';
$this->params['breadcrumbs'][] = ['label' => 'Restaurant', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="restaurant-create">
<div class="col-md-10 col-md-offset-1">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
    <?= $this->render('_form', [
        'restaurant' => $restaurant,
        'allServices' => $allServices,
        'restaurantServices' => $restaurantServices,
    ]) ?>

</div>
