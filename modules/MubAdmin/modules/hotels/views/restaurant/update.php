<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $property app\modules\MubAdmin\modules\hotels\propertys\Property */

$this->title = 'Update restaurant: ' . $restaurant->restaurant_name;
$this->params['breadcrumbs'][] = ['label' => 'Restaurants', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $restaurant->restaurant_name, 'url' => ['view', 'id' => $restaurant->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="restaurant-update">
<div class="col-md-10 col-md-offset-1">

    <h1><?= Html::encode($this->title) ?></h1>
</div>
    <?= $this->render('_form', [
        'restaurant' => $restaurant,
        'allServices' => $allServices,
        'restaurantServices' => $restaurantServices,
    ]) ?>

</div>
