<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use dosamigos\fileupload\FileUploadUI;
use app\helpers\ImageUploader;
/* @var $this yii\web\View */
/* @var $property app\modules\MubAdmin\modules\hotels\propertys\Property */

$this->title = $restaurant->restaurant_name;
$this->params['breadcrumbs'][] = ['label' => 'Restaurants', 'url' => ['index']];
$stateId = \Yii::$app->request->getQueryParam('state');
if($stateId == '')
{
    $stateId = \Yii::$app->request->getBodyParam('state');
}
$this->params['breadcrumbs'][] = $this->title;

$restaurantImages = new app\modules\MubAdmin\modules\hotels\models\RestaurantImages();
$images = $restaurantImages::find()->where(['restaurant_id' => $restaurant->id,'del_status' => '0'])->all();

?>
<style>
    .toggle
    {
        display:none;
    }
</style>
<div class="property-view">
<?= Html::a('Add/View Menu', ['/mub-admin/hotels/menu', 'restaurant' => $restaurant->id,'state' => $stateId], ['class' => 'btn btn-info btn-lg pull-right'])?>
<div class="col-md-10 col-md-offset-1">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
<div class="row">
<div class="col-md-10 col-md-offset-1">
    <?= FileUploadUI::widget([
    'model' => $restaurantImages,
    'attribute' => 'url',
    'url' => ['restaurant/restaurant-image', 'state' => \Yii::$app->request->getQueryParam('state'),'restaurant' => $restaurant->id],
    'gallery' => false,
    'fieldOptions' => [
        'accept' => 'image/*'
    ],
    'clientOptions' => [
        'maxFileSize' => 5000000
    ],
    'clientEvents' => [
        'fileuploaddone' => 'function(e, data) {
                                console.log(e);
                                console.log(data);
                            }',
        'fileuploadfail' => 'function(e, data) {
                                console.log(e);
                                console.log(data);
                            }',
        ],
    ]); ?>
</div>
</div>
<div class="row">
<div class="col-md-10 col-md-offset-1">
    <?php if(!empty($images)){?>
 <?php  foreach($images as $key=>$image){
                    ?>

                    <div id="uploaded<?php echo $key;?>" style="display: inline; margin-right: 10px;">
                    <?php echo Html::img(ImageUploader::resizeRender($image->url, '100', '100'),['height'=>100,'width'=>100]);
                    echo Html::a(
                        Yii::t('app', 'x'), 
                        '#',
                        ["class" => "btn btn-danger realign","onClick" => "deleteFile('".$key."')"]);
                    ?>
                     </div>
                     <?php }}?>
 </div>
</div>
 <div class="row">
    <div class="col-md-5 col-md-offset-1">
<div class="col-md-12" style="margin-top: 2em;">
    <?= DetailView::widget([
        'model' => $restaurant,
        'attributes' => [
            'restaurant_name',
            'restaurant_type',
            'description:ntext',
            'sa_a',
            'sa_b',
           
        ],
    ]) ?>
    </div>
   </div>
</div>
     <div class="row">
        <div class="col-md-5 col-md-offset-1">
     <div class="col-md-12">
        <?= Html::a('Update', ['update', 'id' => $restaurant->id,'state' => $stateId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $restaurant->id,'state' => $stateId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>

        <?= Html::a('All Restaurant', ['index','state' => $stateId], [
            'class' => 'btn btn-primary cancel',
        ]) ?>
     </div>
      </div>
   </div>

</div>