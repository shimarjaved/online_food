<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $property app\modules\MubAdmin\modules\hotels\propertys\PropertySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="property-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($restaurant, 'id') ?>

    <?= $form->field($restaurant, 'mub_user_id') ?>

    <?= $form->field($restaurant, 'rooms_count') ?>

    <?= $form->field($restaurant, 'lat') ?>

    <?= $form->field($restaurant, 'long') ?>

    <?php // echo $form->field($restaurant, 'city_name') ?>

    <?php // echo $form->field($restaurant, 'state_id') ?>

    <?php // echo $form->field($restaurant, 'restaurant_name') ?>

    <?php // echo $form->field($restaurant, 'restaurant_slug') ?>

    <?php // echo $form->field($restaurant, 'restaurant_type') ?>

    <?php // echo $form->field($restaurant, 'restaurant_area') ?>

    <?php // echo $form->field($restaurant, 'sa_a') ?>

    <?php // echo $form->field($restaurant, 'sa_b') ?>

    <?php // echo $form->field($restaurant, 'description') ?>

    <?php // echo $form->field($restaurant, 'pincode') ?>

    <?php // echo $form->field($restaurant, 'created_at') ?>

    <?php // echo $form->field($restaurant, 'updated_at') ?>

    <?php // echo $form->field($restaurant, 'del_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
