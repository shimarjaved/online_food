<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $item app\modules\MubAdmin\modules\hotels\items\ItemSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($item, 'id') ?>

    <?= $form->field($item, 'mub_user_id') ?>

    <?= $form->field($item, 'menu_id') ?>

    <?= $form->field($item, 'item_name') ?>

    <?= $form->field($item, 'item_slug') ?>

    <?php // echo $form->field($item, 'price') ?>

    <?php // echo $form->field($item, 'sell_price') ?>

    <?php // echo $form->field($item, 'discount_price') ?>

    <?php // echo $form->field($item, 'category') ?>

    <?php // echo $form->field($item, 'image_url') ?>

    <?php // echo $form->field($item, 'status') ?>

    <?php // echo $form->field($item, 'created_at') ?>

    <?php // echo $form->field($item, 'updated_at') ?>

    <?php // echo $form->field($item, 'del_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
