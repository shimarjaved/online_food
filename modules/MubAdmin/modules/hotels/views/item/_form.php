<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $item app\modules\MubAdmin\modules\hotels\items\Item */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($item, 'item_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($item, 'price')->textInput() ?>

    <?= $form->field($item, 'sell_price')->textInput() ?>

    <?= $form->field($item, 'discount_price')->textInput() ?>

    <?= $form->field($item, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ], ['prompt' => '']) ?>

    <?= $form->field($item, 'image_url')->fileInput() ?>
    
    <?php echo $form->field($itemCategory, 'category[]')->checkboxList(['breakfast' => 'Breakfast', 'lunch' => 'Lunch', 'dinner' => 'Dinner']) ?>

    <div class="form-group">
        <?= Html::submitButton($item->isNewRecord ? 'Create' : 'Update', ['class' => $item->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
