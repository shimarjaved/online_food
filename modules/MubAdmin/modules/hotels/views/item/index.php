<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\MubAdmin\modules\hotels\models\Menu;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\MubAdmin\modules\hotels\models\ItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Items';
$this->params['breadcrumbs'][] = $this->title;
$stateId = \Yii::$app->request->getQueryParam('state');
if($stateId == '')
{
    $stateId = \Yii::$app->request->getBodyParam('state');
}
$restaurantId = \Yii::$app->request->getQueryParam('restaurant');
$menuId = \Yii::$app->request->getQueryParam('menu');
$menu = Menu::findOne($menuId);

?>
<div class="item-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Item', ['create','restaurant' => $restaurantId,'menu' => $menuId], ['class' => 'btn btn-success']); ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'mub_user_id',
            'menu_id',
            'item_name',
            'item_slug',
            // 'price',
            // 'sell_price',
            // 'discount_price',
            'category_name',
            // 'image_url:url',
            // 'status',
            // 'created_at',
            // 'updated_at',
            // 'del_status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
