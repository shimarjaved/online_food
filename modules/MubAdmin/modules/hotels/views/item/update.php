<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $item app\modules\MubAdmin\modules\hotels\items\Item */

$this->title = 'Update Item: ' . $item->id;
$this->params['breadcrumbs'][] = ['label' => 'Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $item->id, 'url' => ['view', 'id' => $item->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="item-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'item' => $item,
        'itemCategory' => $itemCategory,
    ]) ?>

</div>
