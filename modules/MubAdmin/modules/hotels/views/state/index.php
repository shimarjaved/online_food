<div class="row">
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Cities</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <p>Following are the list of cities we are active for updation</p>
        <table class="table table-striped projects">
          <thead>
            <tr>
              <th style="width: 1%">#</th>
              <th style="width: 20%">City Name</th>
              <th style="width: 20%">#Edit</th>
            </tr>
          </thead>
          <tbody>
          <?php foreach($states as $index => $state){?>
            <tr>
              <td><?=$index+1;?></td>
              <td>
                <a><?=$state->state_name;?></a>
                <br>
                <small>Created <?=$state->created_at;?></small>
              </td>
              <td>
                <a href="property?state=<?=$state->id;?>" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
                <a href="property?state=<?=$state->id;?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                <a href="property?state=<?=$state->id;?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
              </td>
            </tr>
            <?php }?>
          </tbody>
        </table>
        <!-- end project list -->

      </div>
    </div>
  </div>
</div>