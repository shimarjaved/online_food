<?php

namespace app\modules\MubAdmin\modules\hotels\controllers;

use Yii;
use app\modules\MubAdmin\modules\hotels\models\Tenant;
use app\modules\MubAdmin\modules\hotels\models\TenantSearch;
use app\modules\MubAdmin\modules\hotels\models\TenantProcess;
use app\components\MubController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TenantController implements the CRUD actions for Tenant model.
 */
class TenantController extends MubController
{
   public function getPrimaryModel()
   {
        return new Tenant();
   }

   public function getProcessModel()
   {
        return new TenantProcess();
   }

   public function getSearchModel()
   {
        return new TenantSearch();
   }
}
