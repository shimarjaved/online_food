<?php

namespace app\modules\MubAdmin\modules\hotels\controllers;


use app\components\MubController;
use Yii;
use app\modules\MubAdmin\modules\hotels\models\Menu;
use app\modules\MubAdmin\modules\hotels\models\MenuSearch;
use app\modules\MubAdmin\modules\hotels\models\MenuProcess;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MenuController implements the CRUD actions for Menu model.
 */
class MenuController extends MubController
{
    /**
     * @inheritdoc
     */
    public function getPrimaryModel()
   {
          return new Menu();
   }

   public function getProcessModel()
   {
      return new MenuProcess();
   }

   public function getSearchModel()
   {
      return new MenuSearch();
   }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

  }