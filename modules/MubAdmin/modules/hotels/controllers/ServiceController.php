<?php

namespace app\modules\MubAdmin\modules\hotels\controllers;

use Yii;
use app\modules\MubAdmin\modules\hotels\models\Service;
use app\modules\MubAdmin\modules\hotels\models\ServiceProcess;
use app\modules\MubAdmin\modules\hotels\models\ServiceSearch;
use app\components\MubController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ServicesController implements the CRUD actions for Services model.
 */
class ServiceController extends MubController
{
   public function getPrimaryModel()
   {
        return new Service();
   }

   public function getProcessModel()
   {
        return new ServiceProcess();
   }

   public function getSearchModel()
   {
        return new ServiceSearch();
   }
}
