<?php

namespace app\modules\MubAdmin\modules\hotels\controllers;

use Yii;
use app\modules\MubAdmin\modules\hotels\models\Item;
use app\modules\MubAdmin\modules\hotels\models\ItemSearch;
use app\modules\MubAdmin\modules\hotels\models\ItemProcess;
use app\components\MubController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ItemController implements the CRUD actions for Item model.
 */
class ItemController extends MubController
{
   /**
     * @inheritdoc
     */
    public function getPrimaryModel()
   {
          return new Item();
   }

   public function getProcessModel()
   {
      return new ItemProcess();
   }

   public function getSearchModel()
   {
      return new ItemSearch();
   }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

  }