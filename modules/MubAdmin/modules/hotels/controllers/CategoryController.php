<?php
namespace app\modules\MubAdmin\modules\hotels\controllers;
use Yii;
use app\components\MubController;
use app\modules\MubAdmin\modules\hotels\models\Category;
use app\modules\MubAdmin\modules\hotels\models\CategorySearch;
use app\modules\MubAdmin\modules\hotels\models\CategoryProcess;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class CategoryController extends MubController
{
   public function getPrimaryModel()
   {
        return new Category();
   }

   public function getProcessModel()
   {
        return new CategoryProcess();
   }

   public function getSearchModel()
   {
        return new CategorySearch();
   }
}
