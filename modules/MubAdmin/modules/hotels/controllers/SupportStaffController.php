<?php

namespace app\modules\MubAdmin\modules\hotels\controllers;

use Yii;
use app\modules\MubAdmin\modules\hotels\models\PropertySupportStaff;
use app\modules\MubAdmin\modules\hotels\models\PropertySupportStaffSearch;
use app\modules\MubAdmin\modules\hotels\models\SupportStaffProcess;
use app\components\MubController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SupportStaffController implements the CRUD actions for PropertySupportStaff model.
 */
class SupportStaffController extends MubController
{
   public function getPrimaryModel()
   {
        return new PropertySupportStaff();
   }

   public function getProcessModel()
   {
        return new SupportStaffProcess();
   }

   public function getSearchModel()
   {
        return new PropertySupportStaffSearch();
   }
}
