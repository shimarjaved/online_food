<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mub_user_page".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property string $page_title
 * @property string $controller
 * @property string $action
 * @property string $url
 * @property string $page_type
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property MubUser $mubUser
 * @property PageElements[] $pageElements
 * @property PageImages[] $pageImages
 * @property PageSeo[] $pageSeos
 * @property PostComment[] $postComments
 */
class MubUserPage extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mub_user_page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mub_user_id', 'controller', 'action', 'url'], 'required'],
            [['mub_user_id'], 'integer'],
            [['page_type', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['page_title', 'action', 'url'], 'string', 'max' => 100],
            [['controller'], 'string', 'max' => 50],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'mub_user_id' => Yii::t('app', 'Mub User ID'),
            'page_title' => Yii::t('app', 'Page Title'),
            'controller' => Yii::t('app', 'Controller'),
            'action' => Yii::t('app', 'Action'),
            'url' => Yii::t('app', 'Url'),
            'page_type' => Yii::t('app', 'Page Type'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'del_status' => Yii::t('app', 'Del Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageElements()
    {
        return $this->hasMany(PageElements::className(), ['page_id' => 'id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageImages()
    {
        return $this->hasMany(PageImages::className(), ['page_id' => 'id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageSeos()
    {
        return $this->hasMany(PageSeo::className(), ['page_id' => 'id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostComments()
    {
        return $this->hasMany(PostComment::className(), ['page_id' => 'id'])->where(['del_status' => '0']);
    }
}
