<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "homes_listing".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property string $property_type
 * @property string $property_area
 * @property string $owner_name
 * @property string $email
 * @property string $mobile
 * @property string $sa_a
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property MubUser $mubUser
 */
class HomesListing extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'homes_listing';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_name', 'sa_a','email','mobile'], 'required'],
            [['mub_user_id'], 'integer'],
            [['property_type', 'property_area', 'del_status'], 'string'],
            ['email','email'],
            ['mobile','number'],
            ['mobile','string', 'max' => 10 , 'min' => 10],
            [['created_at', 'updated_at'], 'safe'],
            [['owner_name', 'email', 'mobile', 'sa_a'], 'string', 'max' => 255],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'property_type' => 'Property Type',
            'property_area' => 'Property Area',
            'owner_name' => 'Owner Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'sa_a' => 'Sa A',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }
}
