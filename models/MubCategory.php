<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mub_category".
 *
 * @property integer $id
 * @property string $category_name
 * @property string $category_slug
 * @property integer $category_parent
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property MubCategory $categoryParent
 * @property MubCategory[] $mubCategories
 * @property MubPingUrls[] $mubPingUrls
 * @property PostCategory[] $postCategories
 */
class MubCategory extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mub_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_name', 'category_slug'], 'required'],
            [['category_parent'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['del_status'], 'string'],
            [['category_name', 'category_slug'], 'string', 'max' => 50],
            [['category_parent'], 'exist', 'skipOnError' => true, 'targetClass' => MubCategory::className(), 'targetAttribute' => ['category_parent' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_name' => Yii::t('app', 'Category Name'),
            'category_slug' => Yii::t('app', 'Category Slug'),
            'category_parent' => Yii::t('app', 'Category Parent'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'del_status' => Yii::t('app', 'Del Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryParent()
    {
        return $this->hasOne(MubCategory::className(), ['id' => 'category_parent'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubCategories()
    {
        return $this->hasMany(MubCategory::className(), ['category_parent' => 'id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubPingUrls()
    {
        return $this->hasMany(MubPingUrls::className(), ['for_category' => 'id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostCategories()
    {
        return $this->hasMany(PostCategory::className(), ['category_id' => 'id'])->where(['del_status' => '0']);
    }
}
