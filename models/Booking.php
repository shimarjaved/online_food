<?php

namespace app\models;

use Yii;
use app\modules\MubAdmin\modules\RealEstate\models\Property;
/**
 * This is the model class for table "booking".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property integer $property_id
 * @property string $name
 * @property string $email
 * @property string $mobile
 * @property string $booking_id
 * @property string $occupancy
 * @property integer $amount
 * @property integer $actual_price
 * @property integer $tax_amount
 * @property string $payment_mode
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property MubUser $mubUser
 * @property Property $property
 */
class Booking extends \app\components\Model
{

    public $price;
    public $tax;
    public $total;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booking';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mub_user_id', 'property_id', 'name', 'email', 'mobile', 'booking_id','occupancy'], 'required'],
            [['mub_user_id', 'property_id', 'amount', 'actual_price', 'tax_amount'], 'integer'],
             ['price','integer','min' => '0','max' => '99999'],
            ['total','integer','min' => '0','max' => '99999'],
            ['mobile','number'],
            ['mobile','string', 'max' => 13 , 'min' => 10],
            [['occupancy', 'status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'email', 'mobile'], 'string', 'max' => 50],
            [['booking_id', 'payment_mode'], 'string', 'max' => 255],
            [['status', 'del_status','occupancy','booking_id'], 'string'],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => Property::className(), 'targetAttribute' => ['property_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'property_id' => 'Property ID',
            'name' => 'Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'booking_id' => 'Booking ID',
            'occupancy' => 'Occupancy',
            'amount' => 'Amount',
            'actual_price' => 'Actual Price',
            'tax_amount' => 'Tax Amount',
            'payment_mode' => 'Payment Mode',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id' => 'property_id']);
    }
}
