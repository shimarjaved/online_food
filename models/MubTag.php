<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mub_tag".
 *
 * @property integer $id
 * @property string $tag_name
 * @property string $tag_slug
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property PostTags[] $postTags
 */
class MubTag extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mub_tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tag_name', 'tag_slug'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['del_status'], 'string'],
            [['category_id'],'integer'],
            [['tag_name', 'tag_slug'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tag_name' => Yii::t('app', 'Tag Name'),
            'tag_slug' => Yii::t('app', 'Tag Slug'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'del_status' => Yii::t('app', 'Del Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostTags()
    {
        return $this->hasMany(PostTags::className(), ['tag_id' => 'id'])->where(['del_status' => '0']);
    }

    public function getTagCategory()
    {
        return $this->hasOne(MubCategory::className(), ['id' => 'category_id'])->where(['del_status' => '0']);
    }
}
