<?php
namespace app\models;

use app\components\Model;

class SaveEnquiry extends Model
{
    public $name;
    public $email;
    public $mobile;
    public $occupancy;
    public $schedule_time;
     
    public function schedule()
    {
        if (!$this->validate()) {
             p($this->getErrors());
        }
        $transaction = \Yii::$app->db->beginTransaction();
        try 
        {
        $user = new User();
        $user->name = $this->name;
        $user->email = $this->email;
        $user->mobile = $this->mobile;
        $user->occupancy = $this->occupancy;
        $user->schedule_time = $this->schedule_time;
                
        if($user->save())
        {
            $auth = \Yii::$app->authManager;
            $scheduleVisits = new \app\models\ScheduleVisits();
            $scheduleVisits->user_id = $user->id;
            $scheduleVisits->name = $this->name;
            $scheduleVisits->email = $this->email;
            $scheduleVisits->mobile = $this->mobile;
            $scheduleVisits->occupancy = $this->occupancy;
            $scheduleVisits->schedule_time = $this->schedule_time;
            
       }
    }
  }
}
