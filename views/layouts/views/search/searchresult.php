<?php 
   use yii\helpers\Html;
   use \app\helpers\ImageUploader;
   use yii\widgets\LinkPager;
   $propType = [
  'hostel' => 'Hostel',
  'pg' => 'PG',
  'flat' => 'Flat',
  'house' => 'House'
];
$propFor = [
  'boys' => 'Boys',
  'girls' => 'Girls',
  'family' => 'Family',
  'both' => 'Both'
];
$titleFor = (isset($propFor[$result['rooms_for']])) ? $propFor[$result['rooms_for']] : '';
$titleProp = (isset($propType[$result['rooms_type']])) ? $propType[$result['rooms_type']] : 'PG';

$titleLocation = (isset($result['location'])) ? 'in '.str_replace(', India', '', $result['location']) : '';
$this->title = $titleFor.' '.$titleProp.' '.$titleLocation;
   
/* facebook meta tag*/
   $this->registerMetaTag([
      'title' => 'og:title',
      'content' =>'OSMSTAYS Rental', 
   ]);

   $this->registerMetaTag([
      'title' => 'og:keyword',
      'content' =>'Rooms for Rent in Gurgoan, PG in Gurgoan, 1BHK for Rent in Gurgoan, 2BHK for rent in Gurgoan, Flats in Gurgoan, Funished Flats in Gurgoan, Fully Furnished Flats in Gurgoan, Semi-Furnished Flats in Gurgoan,  ', 
   ]);

   $this->registerMetaTag([
      'app_id' => 'fb:app_id',
      'content' => '892362904261944'
   ]);

   $this->registerMetaTag([
      'type' => 'og:type',
      'content' => 'article'
   ]);

   $this->registerMetaTag([
      'url' => 'og:url',
      'content' => 'http://'.$_SERVER['HTTP_HOST'].\Yii::$app->request->url,
   ]);
      $this->registerMetaTag([
         'image' => 'og:image',
         'content' => 'http://'.$_SERVER['HTTP_HOST'].'/uploads/logo.jpg',
      ]);
   $this->registerMetaTag([
      'description' => 'og:description',
      'content' => 'description here'
   ]);

/*close facebook meta tag*/

/*twitter meta tag*/

   $this->registerMetaTag([
      'card' => 'twitter:card',
      'content' => "summury"
   ]);
   $this->registerMetaTag([
      'site' => "twitter:site",
      'content' => "@publisher_handle"
   ]);
   
   $this->registerMetaTag([
      'title' => 'twitter:title',
      'content' => 'OSMSTAYS Property'
   ]);

   $this->registerMetaTag([
      'description' => 'twitter:description',
      'content' => 'Page description less than 200 characters' 
   ]);

   $this->registerMetaTag([
      'creater' => 'twitter:creater',
      'content' => '@author_handle' 
   ]);

   $this->registerMetaTag([
      'url' => 'twitter:url',
      'content' => 'http://'.$_SERVER['HTTP_HOST'].\Yii::$app->request->url,
   ]);
   $this->registerMetaTag([
         'image' => 'twitter:image:src',
         'content' => 'http://'.$_SERVER['HTTP_HOST'].'/uploads/logo.jpg',
      ]);
   ?>
<style type="text/css">
.address p
{
color: #fff!important;
font-size: 14px;
}
hr{
    margin-top: 20px;
    border: 0;
    border-top: 4px solid #F7882F;
  }

@media (max-width: 468px)
{
  .margin-general{margin-top: 4em!important;}}
</style>
<div class="container">
   <div class="container-fluid">
      <div class="row margin-general" style="margin-top: 1em">
         <div class="col-md-8">
            <div class="pull-left" style="margin-left: -1em;">
               <h3>Search Result 
               <span id="search-result-for"> <?=(isset($propFor[$result['rooms_for']])) ? 'For '.$propFor[$result['rooms_for']] : '';?></span>
               <span id="search-room-type"><?=(isset($propType[$result['rooms_type']])) ? $propType[$result['rooms_type']] : '';?></span>

               <span id="search-city-name"><?=(isset($result['location'])) ? 'in '.str_replace(', India', '', $result['location']) : '';?>   
               </span></h3>

            </div>
         </div>
         
         <div class="col-md-4 no-display">
            <div class="col-md-8">
               <div class="text-center" style="margin-left: 2em;">Sort By</div>
            </div>
            <div class="col-md-4">
               <div class="pull-right" style="margin-right:1em;">View By</div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-xs-12 col-md-8 col-md-offset-1 hidden-xs">
            <div class="pull-left" style="margin-left:-7.5em;">
               <p><i class="fa fa-home"></i> > Search result</p>
            </div>
         </div>
         <div class="col-md-3">
            <div class="row">
               <div class="col-md-7 hidden-xs new-line">
                  <div class="pull-right1" style="margin-left: -3.5em;">
                     <select id="sort-search" class="form-control view2">
                        <option selected value="plth">Price Low to High</option>
                        <option value="phtl">Price high to low</option>
                        <!-- <option value="dlth">Dist. Low to High</option> -->
                        <!-- <option value="dhtl">Dist.  High to Low</option> -->
                     </select>
                  </div>
                  </div>
                  <div class="col-xs-7 hidden-sm hidden-lg hidden-mdnew-line">
                   <div class="pull-right1" style="margin-top: 1.2em;">
                     <select id="sort-search" class="form-control view2">
                        <option selected value="dlth">Price Low to High</option>
                        <option value="phtl">Price high to low</option>
                        <option value="dlth">Distance  Low to High</option>
                        <option value="dhtl">Distance  High to Low</option>
                     </select>
                  </div>
               </div>
               <div class="col-md-5 col-xs-5 new-line">
                  <div class="pull-right1">
                     <select id="select-view" class="form-control" style="width: 90%!important;">
                        <option value="grid" selected>Grid</option>
                        <option value="list">List</option>
                     </select>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="page-w3">
      <div class="container-fluid">
         <div class="card-columns row offset-left">
            <span onclick="openNav()" id="open">&#9776; Filter Options</span>
            <form class="col-md-3 card1" id="searchTab">
               <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
               <div>
                  <div class="filter-widget-grid">
                     <div>
                        <div class="form-group offset-left">
                           <div class="title">
                              <h5>Location</h5>
                           </div>
                           <div>
                              <form class="" action="" accept-charset="utf-8" id="">
                                 <div class="form-group">
                                    <input id="searchPlaces" type="text" placeholder="Search by locality" id="sideAutocomplete" class="form-control" autocomplete="off">
                                     <input type="hidden" name="error-search-field" id="error-search-field" value="">
                                    <div id="search-result-msg" class="search-field-result ellipsify" style="text-transform: capitalize; padding: 10px 0px 10px 18px;">
                                       <span class="result-count" id="resultcount">(<?=$result['result_count'];?>)</span>
                                       <span class="keyword" id="cityname"><?=$result['city_name'];?></span>
                                    </div>
                                 </div>
                                 <input type="hidden" name="lat" value="<?=$result['lat'];?>" id="lat">
                                 <input type="hidden" name="long" value="<?=$result['long'];?>"id="long">
                                 <input value="<?=$result['location'];?>" type="hidden" name="location" id="location">
                                 <input type="hidden" name="per_page" id="per_page" value="9">
                                 <input type="hidden" name="city_name" id="city_name" value="<?=$result['city_name'];?>">
                                 <input type="hidden" name="pincode" id="pincode" value="">
                                 <input type="hidden" name="state_name" id="state_name" value="<?=$result['state_name'];?>">
                                 <input type="hidden" name="rooms_for" id="rooms_for" value="<?=$result['rooms_for'];?>">
                                 <input type="hidden" name="rooms_type" id="rooms_type" value="<?=$result['rooms_type'];?>">
                                 <input type="hidden" value="searchresult" id="page_name" name="page_name">
                                 <input type="hidden" value="50000" id="uplimit" name="uplimit">
                                 <input type="hidden" value="<?=$result['displayview']?>" id="displayview" name="displayview">
                                 <input type="hidden" value="2000" id="lowlimit" name="lowlimit">
                                 <input type="hidden" name="locality" id="locality" value="<?=isset($result['locality']) ? $result['locality'] : '';?>">
                              </form>
                           </div>
                        </div>
                        <div id="grid-view">
                        <div class="form-group offset-left">
                           <div class="title">
                              <h5>Accommodation for</h5>
                           </div>
                           <div>
                              <input type="radio" id="gender-girls" name="rooms_for" value="girls" <?php if(isset($result['rooms_for'])){
                                 echo ($result['rooms_for'] =='girls') ? 'checked' : '';}?>>
                              <label for="gender-girls"></label>Girls
                           </div>
                           <div>
                              <input type="radio" id="gender-boys" name="rooms_for" value="boys" class="" <?php if(isset($result['rooms_for'])){
                                 echo ($result['rooms_for'] =='boys') ? 'checked' : '';}?>>
                              <label for="gender-boys"></label>Boys
                           </div>
                           <div>
                              <input type="radio" id="gender-family" name="rooms_for" value="both" <?php if(isset($result['rooms_for'])){
                                 echo ($result['rooms_for'] =='both') ? 'checked' : '';}?>>
                              <label for="gender-boys"></label>Both
                           </div>
                           <div>
                              <input type="radio" id="gender-family" name="rooms_for" value="family" <?php if(isset($result['rooms_for'])){
                                 echo ($result['rooms_for'] =='family') ? 'checked' : '';}?>>
                              <label for="gender-boys"></label>family
                           </div>
                        </div>
                        <div class="form-group offset-left">
                           <div class="title">
                              <h5>Accommodation Type</h5>
                           </div>
                           <div>
                              <input type="radio" name="rooms_type" id="type-bed" value="hostel" class="" <?php if(isset($result['rooms_type'])){
                                 echo ($result['rooms_type'] =='hostel') ? 'checked' : '';}?>>
                              <label for="type-bed" class="disabled-radio"></label>Hostel
                           </div>
                           <div>
                              <input type="radio" name="rooms_type" id="type-room" value="pg" <?php if(isset($result['rooms_type'])){
                                 echo ($result['rooms_type'] =='pg') ? 'checked' : '';}?>>
                              <label for="type-room"></label>PG
                           </div>
                           <div>
                              <input type="radio" name="rooms_type" id="type-house" value="flat" <?php if(isset($result['rooms_type'])){
                                 echo ($result['rooms_type'] =='flat') ? 'checked' : '';}?>>
                              <label for="type-house"></label>Flats
                           </div>
                           <div>
                              <input type="radio" name="rooms_type" id="type-house" value="house" <?php if(isset($result['rooms_type'])){
                                 echo ($result['rooms_type'] =='house') ? 'checked' : '';}?>>
                              <label for="type-house"></label>Independent Rooms
                           </div>
                        </div>
                        <div class="form-group offset-left">
                           <div class="title">
                              <h5>Monthly Budget</h5>
                           </div>
                           <div>
                              <div id="priceinfilter"></div>
                              <br /> Showing houses Between <span name="pricelow" id="pricelow"></span> and <span id="pricehigh" name="pricehigh"></span>
                           </div>
                           
                        </div>
                        <div class="form-group offset-left">
                           <div class="title side-filter-title">
                              <h5 class="">House Tags</h5>
                           </div>
                           <div class="">
                              <div>
                                 <input name="tag_premium"type="checkbox" id="premium" value="premium" checked>
                                 <label class="" for="premium"></label>Premium
                              </div>
                              <div>
                                 <input type="checkbox" name="tag_flagship" id="flagship" value="flagship" checked>
                                 <label class="" for="flagship"></label>FlagShip
                              </div>
                              <div>
                                 <input type="checkbox" name="tag_economy" id="premium" value="economy" checked>
                                 <label class="" for="economy"></label>Economy
                              </div>
                           </div>
                        </div>
                        <div class="form-group offset-left">
                           <h5 class="">Furnishing Type</h5>
                           <div class="">
                              <div>
                                 <input name="fullyfur" type="checkbox" id="fullyfur" checked>
                                 <label class="" for="fullyfur"></label>Fully Furnished
                              </div>
                           </div>
                           <div class="">
                              <div class="">
                                 <input name="semifur" type="checkbox" id="" checked>
                                 <label class="semifur" for="semifur"></label>Semi Furnished
                              </div>
                           </div>
                           <div class="">
                              <div>
                                 <input name="unfur" type="checkbox" id="unfur" checked>
                                 <label class="unfur" for="unfur"></label>Unfurnished
                              </div>
                           </div>
                        </div>
                        <div class="clr"></div>
                        <div class="form-group text-center offset-left">
                           <button class="btn btn-reset filter-btn">Reset Filter</button>
                        </div>
                        
                     </div>
                  </div>

               </div>
                  <h5 class="">Share this result on Social Media</h5><br/>
                         <div class="" style="padding-bottom: 1em;">
                            <a href="https://www.facebook.com/sharer.php?u=<?= 'http://'.$_SERVER['HTTP_HOST'].'/property/property-details?id='?>" target="_blank"><button class="btn btn-primary" style="padding: 0.2em;" ><b>Facebook</button></a>

                           <a href="https://plus.google.com/share?url=<?= 'http://'.$_SERVER['HTTP_HOST'].'/property/property-details?id=';?>" target="_blank"><button class="btn btn-danger" style="padding: 0.2em;"><b>Google +</button></a>

                           <a href="https://twitter.com/share?text=OSMSTAYS Property;?>!&amp;url=<?= 'http://'.$_SERVER['HTTP_HOST'].'/property/property-details?id=';?>" target="_blank"><button class="btn btn-info" style="padding: 0.2em;"><b>Twitter</button></a>
                         </div>
            </div>
            <div class="card col-md-9" id="dynamic-div" style=" padding-right: 0px;">
         
                                             <!-- GRID VIEW -->
         
            <?php if($result['displayview'] == 'grid'){?>
               <div class="company-grids">
               <input type="hidden" name="result_count" id="result_count" value="<?=$result['result_count']?>">
               <?php if($result['result_count'] > 0){?>
                  <?php foreach($result['result'] as $res){ 
                     $propertyImage = ($res->propertyImages) ? $res->propertyImages[0]->url : '/images/notfound.jpg'  ;
                    // \app\helpers\PropertyHelper::updateLowestPrice($res);
                     ?>
                  <div class="col-md-4 company-grid multi-gd-text">
                     <a target="_blank" href="/property/property-details?name=<?=\app\helpers\StringHelper::generateSlug($res->property_name);?>" class="vik" ><?php echo Html::img(ImageUploader::resizeRender($propertyImage, '255', '166'),['class' => 'img-responsive','alt' => $res->sa_b]);?></a>
                     <div class="wid" style="background-color: #aab2bf!important; margin-top: -0.7em!important;"><br/>
                     <h4>
                     <span class="pull-left" style="color: #EEEEEE!important;    padding-left: 0.7em"><?= ucwords($res['property_name']);?></span>
                     </h4>
                     <br />
                     
                     <span title="<?=$res->sa_b;?>">
                     <a target="_blank" class="heigh" href="/property/property-details?name=<?=\app\helpers\StringHelper::generateSlug($res->property_name);?>" style="text-decoration: none!important;"><p style="color: #EEEEEE!important; padding-left: 0.9em;padding-right: 0.7em;"><b><?=substr($res->sa_b ,0 , 26);?>...</b></p></a>
                     </span><br/>
                     <span class="pull-left" style="color: #FFF!important;  font-weight: bold;padding-left: 1.1em;margin-top: -1em;">₹ <?=$res->show_price;?> onwards</span><br/>
                     <h6 class="address p"><p><?=substr(str_replace('&nbsp;', ' ', strip_tags($res->description)) ,0 , 80);?>...</p></h6>

                     <div class="text-center" style="padding-bottom: 0.4em;">
                           <a href="https://www.facebook.com/sharer.php?u=<?= 'http://'.$_SERVER['HTTP_HOST'].'/property/property-details?name='.\app\helpers\StringHelper::generateSlug($res->property_name);?>" target="_blank"><button class="btn btn-primary" style=" padding-left: 0.8em; padding-right: 0.8em; padding-top: 0.3em;"><b>f</button></a>

                           <a href="https://plus.google.com/share?url=<?= 'http://'.$_SERVER['HTTP_HOST'].'/property/property-details?name='.\app\helpers\StringHelper::generateSlug($res->property_name);?>" target="_blank"><button class="btn btn-danger" style="padding-left: 0.5em; padding-right: 0.5em; padding-top: 0.3em;"><b>G+</button></a>

                           <a href="https://twitter.com/share?text=OSMSTAYS Property;?>!&amp;url=<?= 'http://'.$_SERVER['HTTP_HOST'].'/property/property-details?name='.\app\helpers\StringHelper::generateSlug($res->property_name);?>" target="_blank"><button class="btn btn-info" style="padding-left: 0.8em; padding-right: 0.8em; padding-top: 0.3em;" target="_blank"><b>t</button></a>
                     </div>

                     </div>
                  </div>

                  <?php }}else{?>
                  <h2>Sorry There was no result matching your request!</h2>
                  <?php }?>
                  <div class="clearfix"></div>
                  <?php if($result['result_count'] > 0){?>
                  <center><?= LinkPager::widget([
                   'pagination' => $result['pages'],
                   ]);
                  ?></center><?php }?>
               </div>
                                          <!-- LIST VIEW -->

               <?php }else{?>
               
     <?php if($result['result_count'] > 0){?>
      <div class="agent-mub">
      <input type="hidden" name="result_count" id="result_count" value="<?=$result['result_count']?>">
    <?php foreach($result['result'] as $res){ 
      $propertyImage = ($res->propertyImages) ? $res->propertyImages[0]->url : '/images/notfound.jpg' ;
      ?>
    <div style="padding-left:0px" class="col-md-12 agent-grid">
<div class="agent-left multi-gd-text">
      <a target="_blank" href="/property/property-details?name=<?=\app\helpers\StringHelper::generateSlug($res->property_name);?>" class='vik'>
      <?php echo Html::img(ImageUploader::resizeRender($propertyImage, '350', '252'),['class' => 'img-responsive','alt' => $res->sa_b]);?></a>
      </div>
      <div class="agent-right listviewbg">
        <h3 class="prop-desc" style="margin-top: -0.4em!important"><?= ucwords($res['property_name']);?></h3>
        <ul>
          <li class="pull-right hidden-xs price"><h3 class="prop-desc">Price : ₹ <?=$res->show_price;?> onwards</h3></li>
          <li class="hidden-md hidden-sm hidden-lg price2"><h4 class="prop-desc">Price : ₹ <?=$res->show_price;?> onwards</h4></li>
          <a target="_blank" href="/property/property-details?name=<?=\app\helpers\StringHelper::generateSlug($res->property_name);?>" style="text-decoration: none!important;" ><li class="addr"><b><?=str_replace(', India','',$res->sa_b);?></b></li></a>
          <li class="agent-grid p descr"><p><?=substr(str_replace('&nbsp;', ' ', strip_tags($res->description)) ,0 , 150);?>...</p></li>
        </ul>
        <div class="pull-right" style="padding-bottom: 0.4em; margin-top: -0.8em!important;">
                           <a href="https://www.facebook.com/sharer.php?u=<?= 'http://'.$_SERVER['HTTP_HOST'].'/property/property-details?name='.\app\helpers\StringHelper::generateSlug($res->property_name);?>" target="_blank"><button class="btn btn-primary" style=" padding-left: 0.8em; padding-right: 0.8em; padding-top: 0.3em;"><b>f</button></a>

                           <a href="https://plus.google.com/share?url=<?= 'http://'.$_SERVER['HTTP_HOST'].'/property/property-details?name='.\app\helpers\StringHelper::generateSlug($res->property_name);?>" target="_blank"><button class="btn btn-danger" style="padding-left: 0.5em; padding-right: 0.5em; padding-top: 0.3em;"><b>G+</button></a>

                           <a href="https://twitter.com/share?text=OSMSTAYS Property;?>!&amp;url=<?= 'http://'.$_SERVER['HTTP_HOST'].'/property/property-details?name='.\app\helpers\StringHelper::generateSlug($res->property_name);?>" target="_blank"><button class="btn btn-info" style="padding-left: 0.8em; padding-right: 0.8em; padding-top: 0.3em;" target="_blank"><b>t</button></a>
                     </div>
      </div>
  </div>
      <?php }}else{?>
   <h2>Sorry There was no result matching your request!</h2>
   <?php }?>
    <div class="clearfix"></div>
    <?php if($result['result_count'] > 0){?>
   <center><?= LinkPager::widget([
    'pagination' => $result['pages'],
    ]);
   ?></center><?php }?>
   </div>

               <?php }?>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>
