<?php use yii\helpers\Html;?>
<?= p(nl2br(Html::encode($message)));?>
<section class="module page-not-found smargin">
  <div class="container">

    <div class="row">
      <div class="col-lg-6 col-lg-offset-3">
        <h2>404</h2>
        <h3>Page not found.</h3>
        <p>The above error occurred while the Web server was processing your request.<br/>Please contact us if you think this is a server error. Thank you.</p>
        
        <a href="/" class="button button-icon"><i class="fa fa-angle-right"></i>Back to Home</a>
      </div>
    </div><!-- end row -->

  </div>
</section>

    